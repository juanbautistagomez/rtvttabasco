package com.juan.rtvttabasco

import android.os.Bundle
import android.util.Log
import androidx.navigation.ui.AppBarConfiguration
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.widget.Toast
import com.google.firebase.database.*
import com.juan.rtvttabasco.networkstate.Network
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class Emisoras : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var database: DatabaseReference

    var gamesData: ArrayList<Dato> = ArrayList()
    private lateinit var recycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_emisoras)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        recycler = findViewById(R.id.RecyclerV) as RecyclerView
        //setRecycler()

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        if (Network.hayRed(this)) {

        } else {
            Toast.makeText(
                this,
                "No se ha podido establecer una conexion de red",
                Toast.LENGTH_SHORT
            ).show()
        }

        database = FirebaseDatabase.getInstance().getReference()
        /*val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
      //  val navView: NavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_gallery,
                R.id.nav_tabhoy,
                R.id.nav_xeva,
                R.id.nav_acerca
                             x       ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.emisoras, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }*/

    }

    override fun onStart() {
        super.onStart()
        database.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w("tag", "getUser:onCancelled", p0.toException());
            }

            override fun onDataChange(p0: DataSnapshot) {
                var datos = p0.getValue(emisorasModel::class.java)
     var arr = datos?.datos

                Log.d("tag", "getUser:onCancelled "+arr);
                // val values:ArrayList<HashMap<String,Any>> = arrayListOf(p0.getValue() as HashMap<String, Any>) //
        gamesData = (arr as ArrayList<Dato>?)!!
        recycler.adapter = AdapterEmisora(arr as ArrayList<Dato>?)




            }
        })

    }
}
