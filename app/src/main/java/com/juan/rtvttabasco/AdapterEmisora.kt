package com.juan.rtvttabasco

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.squareup.picasso.Picasso
class AdapterEmisora(var gmes: ArrayList<Dato>?) :
    RecyclerView.Adapter<AdapterEmisora.RecyclerViewHolder>() {

    override fun getItemCount(): Int {
        return gmes!!.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindData(gmes, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        var view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.reciclerview, parent, false)
        return RecyclerViewHolder(view)
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textName: TextView = itemView.findViewById(R.id.nameemisora)
        var textclve: TextView = itemView.findViewById(R.id.clve)
        var texphone: TextView = itemView.findViewById(R.id.numberPhone)
        var status: TextView = itemView.findViewById(R.id.textView3)
        var btnReproducir: TextView = itemView.findViewById(R.id.reproducir)
        var oppImage: ImageView = itemView.findViewById(R.id.imgRadio)
        var fondo1: ImageView = itemView.findViewById(R.id.fondo1)

        fun bindData(gmes: ArrayList<Dato>?, position: Int) {

            textName.text = gmes?.get(position)?.nombre.toString()

            textclve.text = gmes?.get(position)?.clve.toString()
            texphone.text = "WhatsApp " + gmes?.get(position)?.phone.toString()
            Picasso.get().load(gmes?.get(position)?.logo).into(oppImage)
            Picasso.get().load(gmes?.get(position)?.fondo1).into(fondo1)
            var reproducir = gmes?.get(position)?.play.toString()
            if (reproducir === "true") {

                btnReproducir.text = "Stop"
            } else {
                btnReproducir.text = "Reproducir"
                var url = gmes?.get(position)?.url.toString()

                btnReproducir.setOnClickListener {
                    btnReproducir.text = "Reproduciendo"
                    status.text="En Reproduccion.."
                    val mediaSource = extractMediaSourceFromUri(Uri.parse(url))
                    val exoPlayer = ExoPlayerFactory.newSimpleInstance(
                        itemView.context, DefaultRenderersFactory(itemView.context)
                        , DefaultTrackSelector(),
                        DefaultLoadControl()
                    )
                    exoPlayer.apply {
                        // AudioAttributes here from exoplayer package !!!
                        val attr = AudioAttributes.Builder().setUsage(C.USAGE_MEDIA)
                            .setContentType(C.CONTENT_TYPE_MUSIC)
                            .build()
                        // In 2.9.X you don't need to manually handle audio focus :D
                        setAudioAttributes(attr, true)
                        prepare(mediaSource)
                        // THAT IS ALL YOU NEED
                        playWhenReady = true
                    }
                }
            }
            //  Log.d("tag", gmes.toString())


        }

        private fun extractMediaSourceFromUri(uri: Uri): MediaSource {
            val userAgent = Util.getUserAgent(itemView.context, "Exo")
            return ExtractorMediaSource.Factory(
                DefaultDataSourceFactory(
                    itemView.context,
                    userAgent
                )
            )
                .setExtractorsFactory(DefaultExtractorsFactory()).createMediaSource(uri)
        }
    }
}
