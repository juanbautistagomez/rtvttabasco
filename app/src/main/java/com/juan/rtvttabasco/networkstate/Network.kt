package com.juan.rtvttabasco.networkstate

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.getSystemService

class Network {
    companion object {
        fun hayRed(activity: AppCompatActivity): Boolean {
            val connectivityManager =
                activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkinfo = connectivityManager.activeNetworkInfo
            return networkinfo != null  && networkinfo.isConnected
        }
    }
}