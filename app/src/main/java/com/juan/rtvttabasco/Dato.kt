package com.juan.rtvttabasco

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Dato {


    var id: String? = null

    var nombre: String? = null

    var clve: String? = null

    var logo: String? = null

    var phone: String? = null

    var url: String? = null

    var play: String? = null
    var fondo1: String? = null

}