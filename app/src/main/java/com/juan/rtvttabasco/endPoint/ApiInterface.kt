package com.juan.rtvttabasco
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiInterface {

    @Headers("Accept: application/json")
    @POST("radiotab-cb8e8/data")
    fun datoEmisora(): Call<emisorasModel>



}