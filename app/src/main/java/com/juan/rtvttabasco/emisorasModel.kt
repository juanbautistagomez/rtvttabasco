package com.juan.rtvttabasco

import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class emisorasModel {

    @SerializedName("datos")
    @Expose
    var datos: List<Dato>? = null

}
